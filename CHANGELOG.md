## 1.0

* Initial release
* Including Java and CLI implementations
* Full unit test coverage
* Build logic added for GitLab

## 1.1

* Updates for unit test coverage
* JavaDoc updates
* Randomization logic updates

# 1.2

* Added unique key for randomization, to fix duplicate String objects
* Maven Central updates

# 1.3

* Updates have been made to the weighed ratings, allowing for values other than the recommended 1 - 5 scale.  A 1 - 5 scale is still recommended.
* With the new weighed ratings process, a quick check is made to try to avoid processing a list that is too large to be weighed/ranked.
* Introduced Rating class to replace innerclass within RandomGenerator class.
* Introduced Utility class for clarity.

#1.4

* randomize() methods have been added to include an int ratingLevel object in the method signature.
* The ratingLevel option provides more granularity than the original useRating Boolean option.  Use of the useRating Boolean is still supported and utilizes the RATING_LEVEL_LOW weighting logic.

#1.5

* random data has been added

#1.6

* minor bug fixes

#1.7

* update issue with logging in Spring Boot application

#1.8

* minor fixes with logging

#1.9

* remove all logging to see if this fixes issue with Spring Boot