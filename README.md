## RandomGenerator

[![pipeline status](https://gitlab.com/johnjvester/RandomGenerator/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/RandomGenerator/commits/master)


> The RandomGenerator project provides the ability to randomly sort a given `java.util.List` object, with 
> the option to return a subset of the original list contents.  A delimited `java.lang.String` object can 
> also be sorted, with the ability to return a subset of the original contents.
>
> When using RandomGenerator with a List object, it is possible to introduce a weighted value 
> to the randomization process by including a `java.lang.Integer` field called "rating" in the List items 
> being processed.  
>
> The project builds to a JAR file, which can be utilized by other Java projects, and also 
> includes a command-line interface (CLI) which can be used to randomly sort a delimited String.

### Getting Started

#### Using via Maven Central

Adding RandomGenerator to your Maven project simply requires adding the following dependency to your project:

~~~~
<dependency>
    <groupId>com.gitlab.johnjvester</groupId>
    <artifactId>random-generator</artifactId>
    <version>1.9</version>
</dependency>
~~~~

#### Using the JAR without Maven Central

Follow the steps below to manually use RandomGenerator in your Java project:

1. Clone this project and execute the `mvn package` command
2. Optionally, navigate to the target folder of the RandomGenerator project
3. Optionally, copy the random-generator-version.jar into a location where your Java project can reference the jar
4. Within your Java project, include the random-generator-version.jar as part of your project

#### Using the CLI

RandomGenerator includes a command-line interface (CLI) that can be used with Java.

Follow the steps listed below to use the command-line interface (CLI):

1. Clone this project and execute the `mvn package` command
2. Make sure Java is in your classpath
3. Navigate to the target folder of the RandomGenerator project
4. Execute `java -jar random-generator-version.jar`, which will provide on-line help regarding the current version.

### JavaDoc

[http://johnjvester.gitlab.io/RandomGenerator-JavaDoc/](http://johnjvester.gitlab.io/RandomGenerator-JavaDoc/)

### About Weighted Values

When randomizing `java.util.List` objects with RandomGenerator, the items in the list can be weighted so that 
there is a better chance for one item to appear over another.  In the case of an example where the list being processed 
is centered around an end-user's preferences.  While the list will still be random, they may prefer some items over 
others.  This can be accomplished using the rating field within each item in the list being processed.

Consider the following list items of vacation destinations, with a 1 - 5 rating by an end-user:

| Destination   | Rating |
|:--------------|-------:|
| Boston, MA    | 2      |
| Las Vegas, NV | 3      |
| Maui, HI      | 5      |
| Miami, FL     | 4      |
| New York, NY  | 1      |
| San Diego, CA | 4      |
| Tampa, FL     | 5      |

When using RandomGenerator to randomize the list of destinations without the rating field, the list would be purely
random - giving each destination an equal chance of being first on the list.  However, if the rating field is included
in the processing, then there will be a better chance of Maui (HI) being selected over New York (NY).

Since version 1.3 of RandomGenerator, the rantings are not limited to 1 to 5 values.

Beginning with version 1.4 of RandomGenerator, randomize() methods have been added to include an int ratingLevel object in 
the method signature.  This allows for usage of the following constants:

~~~~
RATING_LEVEL_OFF = disables the rating functionality.
RATING_LEVEL_LOW = enables the lowest form of weighting for the list items being randomized.
RATING_LEVEL_MEDIUM = enables a more aggressive preference toward selecting higher rated items, providing a four times greater emphasis on higher weighted items over lower weighted items.
RATING_LEVEL_HIGH = enables the most aggressive preference toward selecting higher rated items, providing a forty times greater emphasis on higher weighted items over lower weighted items.
~~~~

The ratingLevel option provides more granularity than the original useRating Boolean option.  Use of the useRating Boolean 
is still supported and utilizes the RATING_LEVEL_LOW weighting logic.

### About `RandomGeneratorData`

New with RandomGenerator 1.5 is the ability to have RandomGenerator return a random list of data via the `RandomGeneratorData` class.  

This functionality is currently limited to standard (English) names with a return `List<String>` size limited to the maximum size of a 
`java.lang.Integer`.  The names can be returned in `NameFormat.FIRST_LAST` (John Doe) format or `NameFormat.LAST_FIRST` (Doe, John) format.

Please review the JavaDocs for additional information on this feature.

Created by [John Vester](https://www.linkedin.com/in/johnjvester), because I truly ♥ writing code.
