package com.gitlab.johnjvester.randomizer;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RandomGeneratorDataTest {

    RandomGeneratorData randomGeneratorData = new RandomGeneratorData();

    @Test
    public void firstNameLastNameTest() throws RandomGeneratorException {
        List<String> sixtySevenFullNamesFirstLast = randomGeneratorData.randomizeFullNames(NameFormat.FIRST_LAST, 67);

        assertEquals(67, sixtySevenFullNamesFirstLast.size());
        assert(sixtySevenFullNamesFirstLast.get(0).indexOf(" ") > 0);
        assert(sixtySevenFullNamesFirstLast.get(66).indexOf(" ") > 0);
    }

    @Test
    public void lastNameFirstNameTest() throws RandomGeneratorException {
        List<String> sixtySevenFullNamesFirstLast = randomGeneratorData.randomizeFullNames(NameFormat.LAST_FIRST, 1973);

        assertEquals(1973, sixtySevenFullNamesFirstLast.size());
        assert(sixtySevenFullNamesFirstLast.get(0).indexOf(", ") > 0);
        assert(sixtySevenFullNamesFirstLast.get(1972).indexOf(", ") > 0);
    }

    @Test
    public void nameFormatException() {
        try {
            randomGeneratorData.randomizeFullNames(null, 1);
            fail();
        } catch (RandomGeneratorException e) {
            assert(e.getErrorCode() == ErrorCode.UNKNOWN_NAME_FORMAT);
        }
    }
}
