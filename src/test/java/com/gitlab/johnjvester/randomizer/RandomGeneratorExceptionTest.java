package com.gitlab.johnjvester.randomizer;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RandomGeneratorExceptionTest {
    @Test
    public void testRandomGeneratorExceptionEmptyConstructor() {
        RandomGeneratorException randomGeneratorException = new RandomGeneratorException();
        assertEquals(ErrorCode.UNKNOWN_ERROR, randomGeneratorException.getErrorCode());
    }

    @Test
    public void testRandomGeneratorExceptionCodeOnly() {
        RandomGeneratorException randomGeneratorException = new RandomGeneratorException(ErrorCode.UNKNOWN_NAME_FORMAT);
        assertEquals(ErrorCode.UNKNOWN_NAME_FORMAT, randomGeneratorException.getErrorCode());
    }

    @Test
    public void testRandomGeneratorExceptionCodeAndMessage() {
        String message = "Some message";
        RandomGeneratorException randomGeneratorException = new RandomGeneratorException(ErrorCode.UNKNOWN_NAME_FORMAT, message);
        assertEquals(ErrorCode.UNKNOWN_NAME_FORMAT, randomGeneratorException.getErrorCode());
        assertEquals(message, randomGeneratorException.getMessage());
    }
}
