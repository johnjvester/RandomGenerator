package com.gitlab.johnjvester.randomizer;

import lombok.Data;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class RandomGeneratorTest {

    private static final Integer ONE = 1;
    private static final Integer TWO = 2;
    private static final Integer THREE = 3;
    private static final Integer FOUR = 4;
    private static final Integer FIVE = 5;
    private static final Integer SIX = 6;

    @Test
    public void standardTest() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 25, false);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList);

        assertNotSame("Objects should be in a different order", testList, resultList);
    }

    @Test
    public void standardTestWithRatingType() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 25, true);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList, true);

        assertNotSame("Objects should be in a different order", testList, resultList);
    }

    @Test
    public void standardTestWithMaxResultsAndRatingLevelHigh() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 25, true);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList, 0, RandomGenerator.RATING_LEVEL_HIGH);

        assertNotSame("Objects should be in a different order", testList, resultList);
    }

    @Test
    public void standardTestWithMaxResultsAndRatingLevelMedium() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 25, true);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList, 0, RandomGenerator.RATING_LEVEL_MEDIUM);

        assertNotSame("Objects should be in a different order", testList, resultList);
    }

    @Test
    public void standardTestWithMaxResultsAndRatingLevelLow() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 25, true);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList, 0, RandomGenerator.RATING_LEVEL_LOW);

        assertNotSame("Objects should be in a different order", testList, resultList);
    }

    @Test
    public void standardTestWithMaxResultsAndRatingLevelOff() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 25, true);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList, 0, RandomGenerator.RATING_LEVEL_OFF);

        assertNotSame("Objects should be in a different order", testList, resultList);
    }

    @Test
    public void fiveResultsTest() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 25, false);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList, new Integer("5"));

        assertTrue(resultList.size() == 5);

        assertNotSame("Objects should be in a different order", testList, resultList);
    }

    @Test
    public void fiveResultsTestWithRatingType() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 25, true);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList, 5, true);

        assertTrue(resultList.size() == 5);

        assertNotSame("Objects should be in a different order", testList, resultList);
    }

    @Test
    public void oneResultTest() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 1, false);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList);

        assertSame("Single size objects should be in the same order", testList, resultList);
    }

    @Test
    public void oneResultTestWithRatingType() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 1, true);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList, true);

        assertSame("Single size objects should be in the same order", testList, resultList);
    }

    @Test
    public void tooManyMaxResultsTest() {
        List<TestObject> testList = new ArrayList<TestObject>();
        generateTestData(testList, 25, false);

        RandomGenerator randomGenerator = new RandomGenerator();

        List<TestObject> resultList = randomGenerator.randomize(testList, new Integer("50"));

        assertNotSame("Objects should be in a different order", testList, resultList);
    }

    @Test
    public void standardStringTest() {
        RandomGenerator randomGenerator = new RandomGenerator();

        String testString = generateTestStringData(randomGenerator.DEFAULT_DELIMITER, 25);

        String resultString = randomGenerator.randomize(testString, randomGenerator.DEFAULT_DELIMITER);

        assertNotSame("Objects should be in a different order", testString, resultString);
    }

    @Test
    public void standardStringTestOnlySeven() {
        RandomGenerator randomGenerator = new RandomGenerator();

        String testString = generateTestStringData(randomGenerator.DEFAULT_DELIMITER, 25);

        String resultString = randomGenerator.randomize(testString, randomGenerator.DEFAULT_DELIMITER, 7);

        assertNotSame("Objects should be in a different order", testString, resultString);
    }

    @Data
    private static class TestObject {
        private String value;
        private Integer rating;

        public TestObject() {

        }
    }

    private static void generateTestData(List<TestObject> testList, int listSize, Boolean useRating) {
        for (int i = 0; i < listSize; i++) {
            TestObject testObject = new TestObject();

            switch (i) {
            case 0:
                testObject.setValue("ONE");
                if (useRating) {
                    testObject.setRating(FIVE);
                }
                break;
            case 1:
                testObject.setValue("TWO");
                if (useRating) {
                    testObject.setRating(FOUR);
                }
                break;
            case 2:
                testObject.setValue("THREE");
                if (useRating) {
                    testObject.setRating(THREE);
                }
                break;
            case 3:
                testObject.setValue("FOUR");
                if (useRating) {
                    testObject.setRating(TWO);
                }
                break;
            case 4:
                testObject.setValue("FIVE");
                if (useRating) {
                    testObject.setRating(ONE);
                }
                break;
            case 5:
                testObject.setValue("SIX");
                if (useRating) {
                    testObject.setRating(TWO);
                }
                break;
            case 6:
                testObject.setValue("SEVEN");
                if (useRating) {
                    testObject.setRating(THREE);
                }
                break;
            case 7:
                testObject.setValue("EIGHT");
                if (useRating) {
                    testObject.setRating(FOUR);
                }
                break;
            case 8:
                testObject.setValue("NINE");
                if (useRating) {
                    testObject.setRating(FIVE);
                }
                break;
            case 9:
                testObject.setValue("TEN");
                if (useRating) {
                    testObject.setRating(FOUR);
                }
                break;
            case 10:
                testObject.setValue("ELEVEN");
                if (useRating) {
                    testObject.setRating(THREE);
                }
                break;
            case 11:
                testObject.setValue("TWELVE");
                if (useRating) {
                    testObject.setRating(TWO);
                }
                break;
            case 12:
                testObject.setValue("THIRTEEN");
                if (useRating) {
                    testObject.setRating(ONE);
                }
                break;
            case 13:
                testObject.setValue("FOURTEEN");
                if (useRating) {
                    testObject.setRating(TWO);
                }
                break;
            case 14:
                testObject.setValue("FIFTEEN");
                if (useRating) {
                    testObject.setRating(THREE);
                }
                break;
            case 15:
                testObject.setValue("SIXTEEN");
                if (useRating) {
                    testObject.setRating(FOUR);
                }
                break;
            case 16:
                testObject.setValue("SEVENTEEN");
                if (useRating) {
                    testObject.setRating(FIVE);
                }
                break;
            case 17:
                testObject.setValue("EIGHTEEN");
                if (useRating) {
                    testObject.setRating(FOUR);
                }
                break;
            case 18:
                testObject.setValue("NINETEEN");
                if (useRating) {
                    testObject.setRating(THREE);
                }
                break;
            case 19:
                testObject.setValue("TWENTY");
                if (useRating) {
                    testObject.setRating(TWO);
                }
                break;
            case 20:
                testObject.setValue("TWENTY-ONE");
                if (useRating) {
                    testObject.setRating(SIX); // To validate default case
                }
                break;
            case 21:
                testObject.setValue("TWENTY-TWO");
                if (useRating) {
                    testObject.setRating(TWO);
                }
                break;
            case 22:
                testObject.setValue("TWENTY-THREE");
                if (useRating) {
                    testObject.setRating(THREE);
                }
                break;
            case 23:
                testObject.setValue("TWENTY-FOUR");
                if (useRating) {
                    testObject.setRating(FOUR);
                }
                break;
            case 24:
                testObject.setValue("TWENTY-FIVE");
                if (useRating) {
                    testObject.setRating(FIVE);
                }
                break;
            }

            testList.add(testObject);
        }
    }

    private static String generateTestStringData(String thisCat, int listSize) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < listSize; i++) {
            switch (i) {
            case 0:
                sb.append("ONE");
                sb.append(thisCat);
                break;
            case 1:
                sb.append("TWO");
                sb.append(thisCat);
                break;
            case 2:
                sb.append("THREE");
                sb.append(thisCat);
                break;
            case 3:
                sb.append("FOUR");
                sb.append(thisCat);
                break;
            case 4:
                sb.append("FIVE");
                sb.append(thisCat);
                break;
            case 5:
                sb.append("SIX");
                sb.append(thisCat);
                break;
            case 6:
                sb.append("SEVEN");
                sb.append(thisCat);
                break;
            case 7:
                sb.append("EIGHT");
                sb.append(thisCat);
                break;
            case 8:
                sb.append("NINE");
                sb.append(thisCat);
                break;
            case 9:
                sb.append("TEN");
                sb.append(thisCat);
                break;
            case 10:
                sb.append("ELEVEN");
                sb.append(thisCat);
                break;
            case 11:
                sb.append("TWELVE");
                sb.append(thisCat);
                break;
            case 12:
                sb.append("THIRTEEN");
                sb.append(thisCat);
                break;
            case 13:
                sb.append("FOURTEEN");
                sb.append(thisCat);
                break;
            case 14:
                sb.append("FIFTEEN");
                sb.append(thisCat);
                break;
            case 15:
                sb.append("SIXTEEN");
                sb.append(thisCat);
                break;
            case 16:
                sb.append("SEVENTEEN");
                sb.append(thisCat);
                break;
            case 17:
                sb.append("EIGHTEEN");
                sb.append(thisCat);
                break;
            case 18:
                sb.append("NINETEEN");
                sb.append(thisCat);
                break;
            case 19:
                sb.append("TWENTY");
                sb.append(thisCat);
                break;
            case 20:
                sb.append("TWENTY-ONE");
                sb.append(thisCat);
                break;
            case 21:
                sb.append("TWENTY-TWO");
                sb.append(thisCat);
                break;
            case 22:
                sb.append("TWENTY-THREE");
                sb.append(thisCat);
                break;
            case 23:
                sb.append("TWENTY-FOUR");
                sb.append(thisCat);
                break;
            case 24:
                sb.append("TWENTY-FIVE");
                sb.append(thisCat);
                break;
            }
        }

        return sb.toString();
    }
}