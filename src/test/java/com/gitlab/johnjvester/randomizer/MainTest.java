package com.gitlab.johnjvester.randomizer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MainTest {
    private final java.io.ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private String currentVersion;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        InputStream is = null;

        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            is = classloader.getResourceAsStream("properties.yml");

            Yaml yaml = new Yaml();

            Map<String, Object> result = (Map<String, Object>) yaml.load(is);

            currentVersion = result.get("version").toString();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioe) {
                    System.out.println(ioe.getMessage());
                }
            }
        }
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void testWithoutAnyArgs() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n";

        String[] args = null;
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 30));
    }

    @Test
    public void testWithDefaultDelimiter() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n"
                + "Randomizing string with default delimiter (~~~)\n"
                + "String to randomize:\n"
                + "One~~~Two~~~Three~~~Four~~~Five~~~\n"
                + "Randomized string:\n";

        String[] args = { "One~~~Two~~~Three~~~Four~~~Five~~~" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 153));
    }

    @Test
    public void testWithAlternateDelimiter() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n"
                + "Randomizing string with custom delimiter (^^^)\n"
                + "String to randomize:\n"
                + "One^^^Two^^^Three^^^Four^^^Five^^^\n"
                + "Randomized string:\n";

        String[] args = { "One^^^Two^^^Three^^^Four^^^Five^^^", "-delimiter", "^^^" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 152));
    }

    @Test
    public void testWithAlternateDelimiterAndReturnSize() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n"
                + "Randomizing string with custom delimiter (^^^), returning only 3 elements\n"
                + "String to randomize:\n"
                + "One^^^Two^^^Three^^^Four^^^Five^^^\n"
                + "Randomized string:\n";

        String[] args = { "One^^^Two^^^Three^^^Four^^^Five^^^", "-delimiter", "^^^", "-returnSize", "3" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 179));
    }

    @Test
    public void testWithReturnSizeAndAlternateDelimiter() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n"
                + "Randomizing string with custom delimiter (^^^), returning only 3 elements\n"
                + "String to randomize:\n"
                + "One^^^Two^^^Three^^^Four^^^Five^^^\n"
                + "Randomized string:\n";

        String[] args = { "One^^^Two^^^Three^^^Four^^^Five^^^", "-returnSize", "3", "-delimiter", "^^^" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 179));
    }

    @Test
    public void testWithReturnSize() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n"
                + "Randomizing string with default delimiter (~~~), returning only 3 elements\n"
                + "String to randomize:\n"
                + "One~~~Two~~~Three~~~Four~~~Five~~~\n"
                + "Randomized string:\n";

        String[] args = { "One~~~Two~~~Three~~~Four~~~Five~~~", "-returnSize", "3" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 180));
    }

    @Test
    public void testWithInvalidArg() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n";

        String[] args = { "One~~~Two~~~Three~~~Four~~~Five~~~", "-invalidArg", "hello" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 30));
    }

    @Test
    public void testWithOneValidAndOneInvalidArg() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n";

        String[] args = { "One~~~Two~~~Three~~~Four~~~Five~~~", "-returnSize", "3", "-invalidArg", "hello" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 30));
    }

    @Test
    public void testWithDataFirstLast() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n"
                + "Returning a random list of full names containing 101 value(s) in FIRST_LAST format.";

        String[] args = { "-randomData", "fullNames", "-nameFormat", "FIRST_LAST", "-returnSize", "101" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 113));
    }

    @Test
    public void testWithDataLastFirst() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n"
                + "Returning a random list of full names containing 101 value(s) in LAST_FIRST format.";

        String[] args = { "-randomData", "fullNames", "-nameFormat", "LAST_FIRST", "-returnSize", "101" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 113));
    }

    @Test
    public void testWithDataInvalidType() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n"
                + "An unexpected error occurred.";

        String[] args = { "-randomData", "fullNames", "-nameFormat", "WRONG_FORMAT", "-returnSize", "101" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 59));
    }

    @Test
    public void testWithDataInvalidArgs() {
        String expectedResult = "\n"
                + "RandomGenerator version " + currentVersion + "\n"
                + "\n";

        String[] args = { "-randomData", "notValid", "-nameFormat", "FIRST_LAST", "-returnSize", "101" };
        Main.main(args);
        String result = outContent.toString();

        assertEquals(expectedResult, result.substring(0, 30));
    }
}
