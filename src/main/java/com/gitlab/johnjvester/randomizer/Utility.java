package com.gitlab.johnjvester.randomizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Utility<T> {
    final static Integer MAX_MULTIPLIER_LOW = 5;
    final static Integer MAX_MULTIPLIER_MEDIUM = 21;
    final static Integer MAX_MULTIPLIER_HIGH = 210;

    String convertListToString(List<T> thisList, String thisCat) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < thisList.size(); i++) {
            sb.append(thisList.get(i).toString());
            sb.append(thisCat);
        }

        return sb.toString();
    }

    List<T> convertStringToList(String thisString, String thisCat) {
        List<T> thisList = new ArrayList<T>();
        int lastCatPointer = 0;

        while (lastCatPointer < thisString.length()) {
            int thisCatPointer = thisString.indexOf(thisCat, lastCatPointer);

            String thisCatValue = thisString.substring(lastCatPointer, thisCatPointer);

            if (thisCatValue != null && thisCatValue.length() > 0) {
                thisList.add((T) thisCatValue);
            }
            lastCatPointer = thisCatPointer + thisCat.length();
        }

        return thisList;
    }

    Boolean isListSizeValid(List<T> thisList, Integer ratingSize) {
        if (thisList != null && thisList.size() > 0) {
            Integer maxSize = thisList.size() * RandomGenerator.RATING_LEVEL_LOW;

            if (ratingSize == RandomGenerator.RATING_LEVEL_MEDIUM) {
                maxSize = thisList.size() * MAX_MULTIPLIER_MEDIUM;
            } else if (ratingSize == RandomGenerator.RATING_LEVEL_HIGH) {
                maxSize = thisList.size() * MAX_MULTIPLIER_HIGH;
            }

            if (maxSize <= Integer.MAX_VALUE) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    Integer findValueInMap(Integer value, TreeMap<Integer, Integer> treeMap) {
        Map.Entry<Integer, Integer> entry = treeMap.floorEntry(value);
        return entry.getValue();
    }

    Integer findValueInRatingMap(Integer value, TreeMap<Integer, Rating> treeMap) {
        Map.Entry<Integer, Rating> entry = treeMap.floorEntry(value);
        return entry.getValue().getMultiplier();
    }

    Integer getMultiplier(int segment, Integer ratingLevel) {
        /* Below is the logic used to handle 20 levels of multipliers, based upon ratingLevel
         Level 3              2    1
         -------------------- --- ---
         01. 0 + 1 = 1        1    1
         02. 1 + 2 = 3        2    1
         03. 3 + 3 = 6        2    1
         04. 6 + 4 = 10       3    1
         05. 10 + 5 = 15      3    2
         06. 15 + 6 = 21      3    2
         07. 21 + 7 = 28      6    2
         08. 28 + 8 = 36      6    2
         09. 36 + 9 = 45      6    3
         10. 45 + 10 = 55     10   3
         11. 55 + 11 = 66     10   3
         12. 66 + 12 = 78     10   3
         13. 78 + 13 = 91     10   4
         14. 91 + 14 = 105    15   4
         15. 105 + 15 = 120   15   4
         16. 120 + 16 = 136   15   4
         17. 136 + 17 = 153   15   5
         18. 153 + 18 = 171   15   5
         19. 171 + 19 = 190   21   5
         20. 190 + 20 = 210   21   5
        */

        switch (segment) {
        case 1:
            return 1;
        case 2:
            switch (ratingLevel) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            }
        case 3:
            switch (ratingLevel) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 6;
            }
        case 4:
            switch (ratingLevel) {
            case 1:
                return 1;
            case 2:
                return 3;
            case 3:
                return 10;
            }
        case 5:
            switch (ratingLevel) {
            case 1:
                return 2;
            case 2:
                return 3;
            case 3:
                return 15;
            }
        case 6:
            switch (ratingLevel) {
            case 1:
                return 2;
            case 2:
                return 3;
            case 3:
                return 21;
            }
        case 7:
            switch (ratingLevel) {
            case 1:
                return 2;
            case 2:
                return 6;
            case 3:
                return 28;
            }
        case 8:
            switch (ratingLevel) {
            case 1:
                return 2;
            case 2:
                return 6;
            case 3:
                return 36;
            }
        case 9:
            switch (ratingLevel) {
            case 1:
                return 3;
            case 2:
                return 6;
            case 3:
                return 45;
            }
        case 10:
            switch (ratingLevel) {
            case 1:
                return 3;
            case 2:
                return 10;
            case 3:
                return 55;
            }
        case 11:
            switch (ratingLevel) {
            case 1:
                return 3;
            case 2:
                return 10;
            case 3:
                return 66;
            }
        case 12:
            switch (ratingLevel) {
            case 1:
                return 3;
            case 2:
                return 10;
            case 3:
                return 78;
            }
        case 13:
            switch (ratingLevel) {
            case 1:
                return 4;
            case 2:
                return 10;
            case 3:
                return 91;
            }
        case 14:
            switch (ratingLevel) {
            case 1:
                return 4;
            case 2:
                return 15;
            case 3:
                return 105;
            }
        case 15:
            switch (ratingLevel) {
            case 1:
                return 4;
            case 2:
                return 15;
            case 3:
                return 120;
            }
        case 16:
            switch (ratingLevel) {
            case 1:
                return 4;
            case 2:
                return 15;
            case 3:
                return 136;
            }
        case 17:
            switch (ratingLevel) {
            case 1:
                return 5;
            case 2:
                return 15;
            case 3:
                return 153;
            }
        case 18:
            switch (ratingLevel) {
            case 1:
                return 5;
            case 2:
                return 15;
            case 3:
                return 171;
            }
        case 19:
            switch (ratingLevel) {
            case 1:
                return 5;
            case 2:
                return 21;
            case 3:
                return 190;
            }
        case 20:
            switch (ratingLevel) {
            case 1:
                return 5;
            case 2:
                return 21;
            case 3:
                return 210;
            }
        default:
            return 1;
        }
    }
}
