package com.gitlab.johnjvester.randomizer;

public enum ErrorCode {
    UNKNOWN_ERROR,
    UNKNOWN_NAME_FORMAT
}
