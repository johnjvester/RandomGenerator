package com.gitlab.johnjvester.randomizer;

import lombok.Data;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class RandomGenerator<T> {
    private static final Integer ZERO = new Integer("0");

    private Utility utility = new Utility();

    /**
     * <p>Default Delimiter is equal to three tilde (~~~) characters.  This is the assumed default
     * delimiter when a delimiter is not specified.</p>
     *
     * @since 1.0
     */
    public static String DEFAULT_DELIMITER = "~~~";

    /**
     * <p>If the List object includes an Integer field called "rating", this value can be used to
     * provide additional weighting to the randomization.</p>
     * <p>Consider the following two items in the list:</p>
     * <ul>
     * <li>name = Apple, rating = 1</li>
     * <li>name = Orange, rating = 5</li>
     * </ul>
     * <p>When using rating in the randomization in the example above, the Orange object will
     * have a greater number of chances than the Apple object has to be selected.  The
     * feature allows certain value to have a greater chance to be selected.</p>
     * <p>A rating scale of 1 to 5 is recommended, but since version 1.3 this is no longer
     * a requirement.</p>
     *
     * @since 1.0
     */
    public static String RATING = "rating";

    /**
     * <p>When using randomize() methods which include the int ratingLevel,
     * the RATING_LEVEL_OFF disables the rating functionality.  This is the same
     * providing a false value to the legacy randomize() methods which included a
     * useRating Boolean object in the signature.</p>
     *
     * @since 1.4
     */
    public static final int RATING_LEVEL_OFF = new Integer("0");

    /**
     * <p>When using randomize() methods which include the int ratingLevel,
     * the RATING_LEVEL_LOW enables the lowest form of weighting for the list
     * items being randomized.</p>
     * <p>Usage of the randomize() methods which include the useRating Boolean
     * set to true will utilize this rating level during the randomization process.</p>
     * <p>This is similar to the original weighting that was provided with versions
     * 1.2 and earlier of RandomGenerator and follows a 1 to 5 ratio from lowest
     * to highest rated items.</p>
     *
     * @since 1.4
     */
    public static final int RATING_LEVEL_LOW = new Integer("1");

    /**
     * <p>When using randomize() methods which include the int ratingLevel,
     * the RATING_LEVEL_MEDIUM enables a more aggressive preference toward
     * selecting higher rated items.</p>
     * <p>When compared to RATING_LEVEL_LOW, there is a four times greater
     * emphasis on higher weighted items over lower weighted items at the most
     * extreme difference in rating scale.</p>
     * <p>Please note - when using randomize() methods which include the
     * Boolean useRating object, the RATING_LEVEL_LOW level of weighting
     * will be utilized, as this is the default rating level.</p>
     *
     * @since 1.4
     */
    public static final int RATING_LEVEL_MEDIUM = new Integer("2");

    /**
     * <p>When using randomize() methods which include the int ratingLevel,
     * the RATING_LEVEL_HIGH enables the most aggressive preference toward
     * selecting higher rated items.</p>
     * <p>When compared to RATING_LEVEL_LOW, there is a forty times greater
     * emphasis on higher weighted items over lower weighted items at the most
     * extreme difference in rating scale.</p>
     *
     * @since 1.4
     */
    public static final int RATING_LEVEL_HIGH = new Integer("3");

    /**
     * Randomizes the elements in a given tList and returns a new
     * List object of the same type.  All items in the original tList
     * will be returned.
     *
     * @param tList List object to randomize
     * @return new List object whose order has been randomized
     * @since 1.0
     */
    public List<T> randomize(List<T> tList) {
        return preProcessing(tList, ZERO, RATING_LEVEL_OFF);
    }

    /**
     * <p>Randomizes the elements in a given tList (which contains a field
     * named "rating"), applies weighting based upon the rating (if enabled),
     * and returns a new List object of the same type.  All items in the
     * original tList will be returned.</p>
     * <p>If the useRating Boolean is set to true, the randomization will be
     * weighted to favor those with a higher rating field value using the
     * level of weighting provided with the RATING_LEVEL_LOW value.</p>
     * <p>If other RATING_LEVEL_* levels are required, please use a
     * randomize() method which includes the int ratingLevel object.</p>
     *
     * @param tList     List object to randomize
     * @param useRating Boolean to indicate if rating field will be used
     * @return new List object whose order has been randomized
     * @since 1.0
     */
    public List<T> randomize(List<T> tList, Boolean useRating) {
        return preProcessing(tList, ZERO, useRating ? RATING_LEVEL_LOW : RATING_LEVEL_OFF);
    }

    /**
     * <p>Randomizes the elements in a given tList (which contains a field
     * named "rating"), applies weighting based upon the rating (if enabled),
     * and returns a new List object of the same type.  All items in the
     * original tList will be returned.</p>
     * <p>If the ratingLevel is equal to RATING_LEVEL_OFF, the rating processing
     * will be ignored.  Otherwise, the RATING_LEVEL_* value will determine the
     * weighting for higher ranked items.</p>
     * <p>For additional information for each RATING_LEVEL_* value, please
     * review the documentation for each level.</p>
     *
     * @param tList the {@link List} to randomize
     * @param ratingLevel the ratingLevel to utilize
     * @return {@link List}
     * @since 1.4
     */
    public List<T> randomize(List<T> tList, int ratingLevel) {
        return preProcessing(tList, ZERO, ratingLevel);
    }

    /**
     * <p>Randomizes the elements in a given tList and returns a new
     * List object of the same type, limited to the number provided in
     * the maxResults object. Passing a maxResults value of zero will
     * return a List object of the same size as the original tList object.</p>
     * <p>If the maxResults value is greater than the tList size, the return
     * List object will match the size of the original tList.</p>
     *
     * @param tList      List object to randomize
     * @param maxResults the size of the return List (specify 0 to return all results)
     * @return new List object whose order has been randomized and limited to the size of the maxResults object
     * @since 1.0
     */
    public List<T> randomize(List<T> tList, Integer maxResults) {
        return preProcessing(tList, maxResults != null ? maxResults : ZERO, RATING_LEVEL_OFF);
    }

    /**
     * <p>Randomizes the elements in a given tList (which contains a field
     * named "rating"), applies weighting based upon the rating (if enabled),
     * and returns a new List object of the same type - limited to the number
     * provided in the maxResults object. Passing a maxResults value of zero
     * will return a List object of the same size as the original tList object.</p>
     * <p>If the maxResults value is greater than the tList size, the return
     * List object will match the size of the original tList.</p>
     * <p>If the useRating Boolean is set to true, the randomization will be
     * weighted to favor those with a higher rating field value using the
     * level of weighting provided with the RATING_LEVEL_LOW value.</p>
     * <p>If other RATING_LEVEL_* levels are required, please use a
     * randomize() method which includes the int ratingLevel object.</p>
     *
     * @param tList      List object to randomize
     * @param maxResults the size of the return List (specify 0 to return all results)
     * @param useRating  Boolean to indicate if rating field will be used
     * @return new List object whose order has been randomized and limited to the size of the maxResults object
     * @since 1.0
     */
    public List<T> randomize(List<T> tList, Integer maxResults, Boolean useRating) {
        return preProcessing(tList, maxResults != null ? maxResults : ZERO, useRating ? RATING_LEVEL_LOW : RATING_LEVEL_OFF);
    }

    /**
     * <p>Randomizes the elements in a given tList (which contains a field
     * named "rating"), applies weighting based upon the rating (if enabled),
     * and returns a new List object of the same type.  All items in the
     * original tList will be returned.</p>
     * <p>If the ratingLevel is equal to RATING_LEVEL_OFF, the rating processing
     * will be ignored.  Otherwise, the RATING_LEVEL_* value will determine the
     * weighting for higher ranked items.</p>
     * <p>For additional information for each RATING_LEVEL_* value, please
     * review the documentation for each level.</p>
     *
     * @param tList       List object to randomize
     * @param maxResults  the size of the return List (specify 0 to return all results)
     * @param ratingLevel int to indicate the level of RATING_WEIGHT to apply
     * @return new List object whose order has been randomized
     * @since 1.4
     */
    public List<T> randomize(List<T> tList, Integer maxResults, int ratingLevel) {
        return preProcessing(tList, maxResults != null ? maxResults : ZERO, ratingLevel);
    }

    /**
     * For a given thisString object that contains multiple elements denoted
     * by a thisString separator, a new String will be returned, randomizing
     * the values from the source String object.
     *
     * @param thisString    String object to randomize
     * @param thisSeparator String value of separator for thisString object
     * @return new String object whose order has been randomized
     * @since 1.0
     */
    public String randomize(String thisString, String thisSeparator) {
        List<T> thisList = utility.convertStringToList(thisString, thisSeparator);
        return utility.convertListToString(preProcessing(thisList, ZERO, RATING_LEVEL_OFF), thisSeparator);
    }

    /**
     * <p>For a given thisString object that contains multiple elements denoted
     * by a thisString separator, a new String will be returned, randomizing
     * the values from the source String object, limited to the number provided in
     * the maxResults object. Passing a maxResults value of zero will
     * return a String object of the same size as the original String object.</p>
     * <p>If the maxResults value is greater than the number of elements within the
     * thisString object, the return String object will match the size of the original
     * thisString.</p>
     *
     * @param thisString    String object to randomize
     * @param thisSeparator String value of separator for thisString object
     * @param maxResults    the size of the return String (specify 0 to return all results)
     * @return new String object whose order has been randomized and limited to the size of the maxResults object
     * @since 1.0
     */
    public String randomize(String thisString, String thisSeparator, Integer maxResults) {
        List<T> thisList = utility.convertStringToList(thisString, thisSeparator);
        return utility.convertListToString(preProcessing(thisList, maxResults != null ? maxResults : ZERO, RATING_LEVEL_OFF), thisSeparator);
    }

    private List<T> preProcessing(List<T> tList, Integer maxResults, Integer ratingLevel) {
        List<RandomListItem> returnList = new ArrayList<RandomListItem>();

        if (tList != null && tList.size() > 0) {
            if (tList.size() == 1) {
                return tList;
            } else {
                if (ratingLevel != null && ratingLevel > 0 && !utility.isListSizeValid(tList, ratingLevel)) {
                    ratingLevel = 0;
                }

                if (maxResults.intValue() > ZERO) {
                    if (maxResults.intValue() >= tList.size()) {
                        handleRandomization(expandListForRatings(tList, ratingLevel), returnList, ZERO);
                    } else {
                        handleRandomization(expandListForRatings(tList, ratingLevel), returnList, maxResults);
                    }
                } else {
                    handleRandomization(expandListForRatings(tList, ratingLevel), returnList, ZERO);
                }
            }
        }

        return convertToGenericList(returnList);
    }

    private void handleRandomization(List<RandomListItem> randomListItems, List<RandomListItem> returnList, Integer maxResults) {
        while (randomListItems.size() > 0 && (maxResults.intValue() == ZERO || maxResults.intValue() > returnList.size())) {
            int listSize = randomListItems.size();

            int winner = 0;
            if (listSize != 1) {
                Random random = new Random();
                winner = random.nextInt(listSize);
            }

            RandomListItem randomListItem = randomListItems.get(winner);

            returnList.add(randomListItem);
            removeSimilarItems(randomListItems, randomListItem.randomInt);

        }
    }

    private List<RandomListItem> expandListForRatings(List<T> tList, Integer ratingLevel) {
        TreeMap<Integer, Rating> ratingMap = new TreeMap<>();

        List<RandomListItem> newList = new ArrayList<>();

        if (tList != null && tList.size() > 0) {
            if (ratingLevel != null && ratingLevel > 0) {
                ratingMap = processRatingMap(tList, ratingLevel);
            }

            for (T tItem : tList) {
                if (ratingLevel != null && ratingLevel > 0) {
                    copyListItem(tItem, utility.findValueInRatingMap(getThisRating(tItem), ratingMap), newList);
                } else {
                    copyListItem(tItem, 1, newList);
                }
            }
        }

        return newList;
    }

    private Integer getThisRating(T tItem) {
        Integer rating = 1;

        for (Field field : tItem.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.getName().equals(RATING)) {
                if (field.getType().equals(Integer.class)) {
                    try {
                        Object o = field.get(tItem);
                        rating = (Integer) o;
                    } catch (Exception e) {
                        // NOOP
                    }
                }
            }
        }

        return rating;
    }

    private void copyListItem(T thisT, Integer rating, List<RandomListItem> newList) {
        int uniqueId = getUniqueId(newList);

        for (int i = 0; i < rating; i++) {
            RandomListItem randomListItem = new RandomListItem(uniqueId, thisT);
            newList.add(randomListItem);
        }
    }

    private int getUniqueId(List<RandomListItem> newList) {
        Random random = new Random();
        int uniqueId = random.nextInt(2147101773);
        boolean uniqueValueFound = true;

        while (uniqueValueFound) {
            uniqueId = random.nextInt(2147101773);
            uniqueValueFound = foundInList(uniqueId, newList);
        }

        return uniqueId;
    }

    private boolean foundInList(final int uniqueId, List<RandomListItem> newList) {
        return newList.stream()
                .anyMatch(rli -> rli.getRandomInt() == uniqueId);
    }

    private List<T> convertToGenericList(List<RandomListItem> randomListItems) {
        List<T> tList = new ArrayList<>();

        for (RandomListItem randomListItem : randomListItems) {
            tList.add(randomListItem.thisT);
        }

        return tList;
    }

    private void removeSimilarItems(List<RandomListItem> randomListItems, int thisRandomInt) {
        Iterator<RandomListItem> itemIterator = randomListItems.iterator();

        int count = 0;
        while (itemIterator.hasNext()) {
            RandomListItem randomListItem = itemIterator.next();

            if (randomListItem.randomInt == thisRandomInt) {
                itemIterator.remove();
                count++;
            }
        }
    }

    private TreeMap<Integer, Rating> processRatingMap(List<T> tList, Integer ratingLevel) {
        TreeMap<Integer, Rating> ratingMap = buildRatingMap(tList);
        Integer minimumRating = Collections.min(ratingMap.keySet());
        Integer maximumRating = Collections.max(ratingMap.keySet());
        Integer range = maximumRating - minimumRating;
        Integer segmentSize;

        if (range >= 20) {
            segmentSize = range / 20;
        } else {
            segmentSize = 1;
        }

        TreeMap<Integer, Integer> treeMap = getMultiplier(minimumRating, segmentSize, ratingLevel);

        for (Map.Entry<Integer, Rating> entry : ratingMap.entrySet()) {
            Rating rating = entry.getValue();
            rating.setMultiplier(utility.findValueInMap(rating.getValue(), treeMap));
        }

        return ratingMap;
    }

    private TreeMap<Integer, Rating> buildRatingMap(List<T> tList) {
        TreeMap<Integer, Rating> returnMap = new TreeMap();

        if (tList != null && tList.size() > 0) {
            for (T tItem : tList) {
                Integer thisRating = getThisRating(tItem);
                if (!returnMap.containsKey(thisRating)) {
                    Rating rating = new Rating(thisRating);
                    returnMap.put(thisRating, rating);
                }
            }
        }

        return returnMap;
    }

    private TreeMap<Integer, Integer> getMultiplier(Integer startPoint, Integer segmentSize, Integer ratingLevel) {
        TreeMap<Integer, Integer> returnMap = new TreeMap();

        Integer keyValue = startPoint;

        for (int i = 1; i <= 20; i++) {
            returnMap.put(keyValue, utility.getMultiplier(i, ratingLevel));
            keyValue = keyValue + segmentSize;
        }

        return returnMap;
    }

    @Data
    private class RandomListItem {
        public RandomListItem(int randomInt, T thisT) {
            this.randomInt = randomInt;
            this.thisT = thisT;
        }

        private int randomInt;
        private T thisT;
    }
}

