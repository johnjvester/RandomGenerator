package com.gitlab.johnjvester.randomizer;

import java.util.ArrayList;
import java.util.List;

/**
 * Static data objects.
 *
 * @since 1.5
 */
public class RandomData {
    /**
     * Returns a list of unique first names.
     *
     * @since 1.5
     *
     * @return {@link List} of {@link String} objects
     */
    public static List<String> getFirstNames() {
        List<String> list = new ArrayList<>();

        list.add("Abhi");
        list.add("Adam");
        list.add("Alex");
        list.add("Alice");
        list.add("Audrey");

        list.add("Bella");
        list.add("Billie");
        list.add("Bob");
        list.add("Bobby");
        list.add("Brad");

        list.add("Brian");

        list.add("Carol");
        list.add("Cassidy");
        list.add("Chang");
        list.add("Chris");
        list.add("Christie");
        list.add("Cindy");
        list.add("Craig");

        list.add("Dallas");
        list.add("Daly");
        list.add("Dana");
        list.add("Daniel");
        list.add("Danyel");
        list.add("Dayna");
        list.add("Darren");
        list.add("Dave");
        list.add("David");
        list.add("Debbie");
        list.add("Denver");
        list.add("Drew");
        list.add("Dylan");

        list.add("Eduardo");
        list.add("Eric");
        list.add("Ethan");

        list.add("Finnegan");

        list.add("Gail");
        list.add("Greg");
        list.add("Gretchen");

        list.add("Harshini");
        list.add("Heather");
        list.add("Hershell");

        list.add("Ila");
        list.add("Indira");

        list.add("Jackie");
        list.add("James");
        list.add("Jan");
        list.add("Jeff");
        list.add("Jim");
        list.add("John");
        list.add("Jon");
        list.add("Jonathan");
        list.add("June");
        list.add("Justin");

        list.add("Kalyani");
        list.add("Karen");
        list.add("Kristen");

        list.add("Laverne");
        list.add("Lisa");
        list.add("Liz");

        list.add("Marsha");
        list.add("Maureen");
        list.add("Mia");
        list.add("Michelle");

        list.add("Natalie");
        list.add("Nicole");

        list.add("Paducah");
        list.add("Pamela");
        list.add("Peter");
        list.add("Phoenix");
        list.add("Praveena");
        list.add("Preetham");

        list.add("Randie");
        list.add("Rick");
        list.add("Rodney");
        list.add("Ronnie");
        list.add("Russell");

        list.add("Sam");
        list.add("Sarah");
        list.add("Seth");
        list.add("Shirley");
        list.add("Stacy");
        list.add("Sydney");

        list.add("Tamara");
        list.add("Tina");
        list.add("Tim");
        list.add("Todd");
        list.add("Toni");
        list.add("Tracy");
        list.add("Trevor");

        list.add("Verona");
        list.add("Veronica");

        list.add("William");

        list.add("Zach");

        return list;
    }

    /**
     * Returns a list of unique last names.
     *
     * @since 1.5
     *
     * @return {@link List} of {@link String} objects
     */
    public static List<String> getLastNames() {
        List<String> list = new ArrayList<>();

        list.add("Adams");
        list.add("Allen");
        list.add("Anderson");
        list.add("Andrews");

        list.add("Bailey");
        list.add("Baker");
        list.add("Barnes");
        list.add("Bell");
        list.add("Brown");
        list.add("Burch");

        list.add("Coleman");
        list.add("Cook");
        list.add("Cooper");

        list.add("Davis");

        list.add("Foster");

        list.add("Gray");
        list.add("Green");

        list.add("Hall");
        list.add("Hill");
        list.add("Henderson");

        list.add("Jenkins");
        list.add("Johnson");
        list.add("Jones");

        list.add("King");

        list.add("Lee");
        list.add("Li");
        list.add("Long");

        list.add("Marshall");
        list.add("Miller");
        list.add("Mills");
        list.add("Moore");
        list.add("Morgan");
        list.add("Morris");
        list.add("Murphy");

        list.add("Perry");
        list.add("Powell");
        list.add("Price");

        list.add("Reed");
        list.add("Rogers");

        list.add("Scott");
        list.add("Smith");
        list.add("Stewart");

        list.add("Taylor");
        list.add("Thomas");
        list.add("Thompson");

        list.add("Walsh");
        list.add("Williams");
        list.add("Wong");
        list.add("Wood");
        list.add("Wright");

        list.add("Young");

        return list;
    }
}
