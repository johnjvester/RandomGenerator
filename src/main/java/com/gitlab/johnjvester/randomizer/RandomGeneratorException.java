package com.gitlab.johnjvester.randomizer;

import lombok.Data;

@Data
public class RandomGeneratorException extends Exception {
    private ErrorCode errorCode;
    private String message;

    public RandomGeneratorException() {
        this.errorCode = ErrorCode.UNKNOWN_ERROR;
    }

    public RandomGeneratorException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public RandomGeneratorException(ErrorCode errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }
}
