package com.gitlab.johnjvester.randomizer;

import lombok.Data;

@Data
public class Rating {
    public Rating(Integer value) {
        this.value = value;
    }

    private Integer value;
    private Integer multiplier;
}
