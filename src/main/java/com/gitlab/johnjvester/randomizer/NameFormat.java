package com.gitlab.johnjvester.randomizer;

public enum NameFormat {
    FIRST_LAST,
    LAST_FIRST
}
