package com.gitlab.johnjvester.randomizer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class RandomGeneratorData {
    RandomGenerator randomGenerator = new RandomGenerator();

    /**
     * For a given {@link Integer} returnSize, return a list of random full names using
     * the specified {@link NameFormat} nameFormat.
     *
     * @since 1.5
     * @param nameFormat {@link NameFormat} nameFormat to utilize
     * @param returnSize {@link Integer} number of names to return
     * @return {@link List} of {@link String} objects
     * @throws RandomGeneratorException when the {@link NameFormat} cannot be determined
     */
    public List<String> randomizeFullNames(NameFormat nameFormat, Integer returnSize) throws RandomGeneratorException {
        List<String> fullNames = new ArrayList<>();

        List<String> firstNames = randomGenerator.randomize(getFirstNames(returnSize));
        List<String> lastNames = randomGenerator.randomize(getLastNames(returnSize));

        for (int i = 0; i < (returnSize); i++) {
            String firstName = firstNames.get(i);
            String lastName = lastNames.get(i);

            if (nameFormat != null) {
                if (nameFormat.equals(NameFormat.FIRST_LAST)) {
                    fullNames.add(firstName + " " + lastName);
                } else if (nameFormat.equals(NameFormat.LAST_FIRST)) {
                    fullNames.add(lastName + ", " + firstName);
                }
            } else {
                throw new RandomGeneratorException(ErrorCode.UNKNOWN_NAME_FORMAT, "The nameformat parameter cannot be null.");
            }
        }

        return fullNames;
    }

    private List<String> getFirstNames(Integer returnSize) {
        List<String> firstNames = RandomData.getFirstNames();

        if (returnSize > firstNames.size()) {
            double value = returnSize / firstNames.size();

            for (int i = 0; i < (int) value; i++) {
                firstNames.addAll(RandomData.getFirstNames());
            }
        }

        return firstNames.stream().limit(returnSize).collect(Collectors.toList());
    }

    private List<String> getLastNames(Integer returnSize) {
        List<String> lastNames = RandomData.getLastNames();

        if (returnSize > lastNames.size()) {
            double value = returnSize / lastNames.size();

            for (int i = 0; i < (int) value; i++) {
                lastNames.addAll(RandomData.getLastNames());
            }
        }

        return lastNames.stream().limit(returnSize).collect(Collectors.toList());
    }
}
